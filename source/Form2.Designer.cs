﻿namespace minroute
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.OtherPoints_ComboBox = new System.Windows.Forms.ComboBox();
            this.PointName_Label = new System.Windows.Forms.Label();
            this.PointName_TextBox = new System.Windows.Forms.TextBox();
            this.OtherPoints_Label = new System.Windows.Forms.Label();
            this.Distance_TextBox = new System.Windows.Forms.TextBox();
            this.Distance_Label = new System.Windows.Forms.Label();
            this.PointAddress_Label = new System.Windows.Forms.Label();
            this.PointAddress_TextBox = new System.Windows.Forms.TextBox();
            this.Coords_Label = new System.Windows.Forms.Label();
            this.Coords_TextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // OtherPoints_ComboBox
            // 
            this.OtherPoints_ComboBox.Location = new System.Drawing.Point(50, 150);
            this.OtherPoints_ComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.OtherPoints_ComboBox.Name = "OtherPoints_ComboBox";
            this.OtherPoints_ComboBox.Size = new System.Drawing.Size(150, 28);
            this.OtherPoints_ComboBox.TabIndex = 1;
            this.OtherPoints_ComboBox.SelectedIndexChanged += new System.EventHandler(this.OtherPoints_ComboBox_SelectionChangeCommitted);
            this.OtherPoints_ComboBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OtherPoints_ComboBox_KeyPress);
            // 
            // PointName_Label
            // 
            this.PointName_Label.AutoSize = true;
            this.PointName_Label.Location = new System.Drawing.Point(46, 25);
            this.PointName_Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.PointName_Label.Name = "PointName_Label";
            this.PointName_Label.Size = new System.Drawing.Size(110, 20);
            this.PointName_Label.TabIndex = 1;
            this.PointName_Label.Text = "Обозначение";
            // 
            // PointName_TextBox
            // 
            this.PointName_TextBox.Location = new System.Drawing.Point(50, 50);
            this.PointName_TextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.PointName_TextBox.Name = "PointName_TextBox";
            this.PointName_TextBox.Size = new System.Drawing.Size(150, 26);
            this.PointName_TextBox.TabIndex = 0;
            // 
            // OtherPoints_Label
            // 
            this.OtherPoints_Label.AutoSize = true;
            this.OtherPoints_Label.Location = new System.Drawing.Point(46, 125);
            this.OtherPoints_Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.OtherPoints_Label.Name = "OtherPoints_Label";
            this.OtherPoints_Label.Size = new System.Drawing.Size(119, 20);
            this.OtherPoints_Label.TabIndex = 3;
            this.OtherPoints_Label.Text = "Другие пункты";
            // 
            // Distance_TextBox
            // 
            this.Distance_TextBox.Location = new System.Drawing.Point(250, 150);
            this.Distance_TextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Distance_TextBox.Name = "Distance_TextBox";
            this.Distance_TextBox.Size = new System.Drawing.Size(150, 26);
            this.Distance_TextBox.TabIndex = 2;
            this.Distance_TextBox.Visible = false;
            this.Distance_TextBox.TextChanged += new System.EventHandler(this.Distance_TextBox_TextChanged);
            // 
            // Distance_Label
            // 
            this.Distance_Label.AutoSize = true;
            this.Distance_Label.Location = new System.Drawing.Point(246, 125);
            this.Distance_Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Distance_Label.Name = "Distance_Label";
            this.Distance_Label.Size = new System.Drawing.Size(126, 20);
            this.Distance_Label.TabIndex = 5;
            this.Distance_Label.Text = "Расстояние до ";
            this.Distance_Label.Visible = false;
            // 
            // PointAddress_Label
            // 
            this.PointAddress_Label.AutoSize = true;
            this.PointAddress_Label.Location = new System.Drawing.Point(246, 25);
            this.PointAddress_Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.PointAddress_Label.Name = "PointAddress_Label";
            this.PointAddress_Label.Size = new System.Drawing.Size(57, 20);
            this.PointAddress_Label.TabIndex = 7;
            this.PointAddress_Label.Text = "Адрес";
            // 
            // PointAddress_TextBox
            // 
            this.PointAddress_TextBox.Location = new System.Drawing.Point(250, 50);
            this.PointAddress_TextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.PointAddress_TextBox.Name = "PointAddress_TextBox";
            this.PointAddress_TextBox.Size = new System.Drawing.Size(150, 26);
            this.PointAddress_TextBox.TabIndex = 3;
            // 
            // Coords_Label
            // 
            this.Coords_Label.AutoSize = true;
            this.Coords_Label.Location = new System.Drawing.Point(46, 225);
            this.Coords_Label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Coords_Label.Name = "Coords_Label";
            this.Coords_Label.Size = new System.Drawing.Size(188, 20);
            this.Coords_Label.TabIndex = 9;
            this.Coords_Label.Text = "Координаты С.Ш. и В.Д.";
            // 
            // Coords_TextBox
            // 
            this.Coords_TextBox.Location = new System.Drawing.Point(50, 250);
            this.Coords_TextBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Coords_TextBox.Name = "Coords_TextBox";
            this.Coords_TextBox.Size = new System.Drawing.Size(350, 26);
            this.Coords_TextBox.TabIndex = 4;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(434, 311);
            this.Controls.Add(this.Coords_Label);
            this.Controls.Add(this.Coords_TextBox);
            this.Controls.Add(this.PointAddress_Label);
            this.Controls.Add(this.PointAddress_TextBox);
            this.Controls.Add(this.Distance_Label);
            this.Controls.Add(this.Distance_TextBox);
            this.Controls.Add(this.OtherPoints_Label);
            this.Controls.Add(this.PointName_TextBox);
            this.Controls.Add(this.PointName_Label);
            this.Controls.Add(this.OtherPoints_ComboBox);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Add/Edit";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form2_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox OtherPoints_ComboBox;
        private System.Windows.Forms.Label PointName_Label;
        private System.Windows.Forms.TextBox PointName_TextBox;
        private System.Windows.Forms.Label OtherPoints_Label;
        private System.Windows.Forms.TextBox Distance_TextBox;
        private System.Windows.Forms.Label Distance_Label;
        private System.Windows.Forms.Label PointAddress_Label;
        private System.Windows.Forms.TextBox PointAddress_TextBox;
        private System.Windows.Forms.Label Coords_Label;
        private System.Windows.Forms.TextBox Coords_TextBox;
    }
}