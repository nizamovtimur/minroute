﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace minroute
{
    public partial class Form2 : Form
    {
        public Form2(int selected_index)
        {
            this.MaximizeBox = false;
            InitializeComponent();
            OtherPoints_ComboBox.Items.AddRange(form.all_points.ToArray());
            index = selected_index;
            if (index == -1) 
            {
                temp_all_dist = new double[form.all_points.Count + 1];
                for (int i = 0; i < temp_all_dist.Length; i++)
                    temp_all_dist[i] = double.PositiveInfinity;
            }
            else
            {
                PointName_TextBox.Text = form.all_points[index];
                if (form.all_points_info.Count != 0)
                {
                    Coords_TextBox.Text = form.all_points_info[index][2];
                    PointAddress_TextBox.Text = form.all_points_info[index][1];
                }
                temp_all_dist = form.all_distances[index].ToArray();
            }
            this.ClientSize = new Size(450, 300);
        }

        int index;
        static Form1 form = Program.form1;
        double[] temp_all_dist;

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (PointName_TextBox.Text != "")
                if (MessageBox.Show("Хотите ли вы сохранить изменения/добавления?", "Выход", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    SaveMe();
        }
        private void SaveMe()
        {
            form.Output_TextBox.Text = "";
            if (index == -1) // add
            {
                List<double> temp_list = new List<double>();
                for (int i = 0; i < form.all_distances.Count; i++)
                {
                    form.all_distances[i].Add(temp_all_dist[i]);
                    temp_list.Add(temp_all_dist[i]);
                }
                temp_list.Add(temp_all_dist[temp_all_dist.Length - 1]);
                form.all_distances.Add(temp_list);
                form.all_points.Add(PointName_TextBox.Text);
                if (form.Address_CheckBox.Checked)
                    if (PointAddress_TextBox.Text != "")
                        form.Points_CheckedListBox.Items.Add(PointAddress_TextBox.Text, form.Selection_CheckBox.Checked);
                    else
                        form.Points_CheckedListBox.Items.Add("\"" + PointName_TextBox.Text + "\"", form.Selection_CheckBox.Checked);
                else
                    form.Points_CheckedListBox.Items.Add(PointName_TextBox.Text, form.Selection_CheckBox.Checked);
                string[] temp_array = new string[3];
                temp_array[0] = PointName_TextBox.Text;
                if (PointAddress_TextBox.Text != "")
                    temp_array[1] = PointAddress_TextBox.Text;
                else
                    temp_array[1] = $"\"{PointName_TextBox.Text}\"";
                string[] coords = Coords_TextBox.Text.Split(',');
                if (coords.Length != 2)
                    Coords_TextBox.Text = "";
                else if (!double.TryParse(coords[0].Replace(".", ","), out double resx) || !double.TryParse(coords[1].Replace(".", ","), out double resy))
                    Coords_TextBox.Text = "";
                temp_array[2] = Coords_TextBox.Text;
                form.all_points_info.Add(temp_array);
                form.ExportExcel_ToolStripMenuItem.Visible = true;
                form.ExportExcel_ToolStripMenuItem.Enabled = true;
                form.Edit_ToolStripMenuItem.Visible = true;
                form.Delete_ToolStripMenuItem.Visible = true;
                form.BuildRoute_Button.Enabled = true;
            }
            else // edit
            {
                form.all_points[index] = PointName_TextBox.Text;
                if (form.Address_CheckBox.Checked)
                    if (PointAddress_TextBox.Text != "")
                        form.Points_CheckedListBox.Items[index] = PointAddress_TextBox.Text;
                    else
                        form.Points_CheckedListBox.Items[index] = "\"" + PointName_TextBox.Text + "\"";
                else
                    form.Points_CheckedListBox.Items[index] = PointName_TextBox.Text;

                form.all_distances[index] = temp_all_dist.ToList();
                if (form.all_points_info.Count != 0)
                {
                    form.all_points_info[index][0] = PointName_TextBox.Text;
                    form.all_points_info[index][1] = PointAddress_TextBox.Text;
                    string[] coords = Coords_TextBox.Text.Split(',');
                    if (coords.Length != 2)
                        Coords_TextBox.Text = "";
                    else
                    if (!double.TryParse(coords[0].Replace(".", ","), out double resx) || !double.TryParse(coords[1].Replace(".", ","), out double resy))
                        Coords_TextBox.Text = "";
                    form.all_points_info[index][2] = Coords_TextBox.Text;
                }
            }
            form.Output_TextBox.Text = "";
            form.Look2gis_ToolStripMenuItem.Visible = false;
            form.ExportText_ToolStripMenuItem.Visible = false;
        }

        private void Distance_TextBox_TextChanged(object sender, EventArgs e)
        {
            if (double.TryParse(Distance_TextBox.Text, out double res))
                temp_all_dist[OtherPoints_ComboBox.SelectedIndex] = res;
            else
                temp_all_dist[OtherPoints_ComboBox.SelectedIndex] = double.PositiveInfinity;
        }

        private void OtherPoints_ComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (OtherPoints_ComboBox.SelectedIndex != index)
            {
                Distance_Label.Text = "Расстояние до " + OtherPoints_ComboBox.SelectedItem;
                Distance_Label.Visible = true;
                Distance_TextBox.Text = temp_all_dist[OtherPoints_ComboBox.SelectedIndex].ToString();
                Distance_TextBox.Visible = true;
            }
            else
            {
                Distance_Label.Visible = false;
                Distance_TextBox.Visible = false;
            }
        }
        private void OtherPoints_ComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }
}
