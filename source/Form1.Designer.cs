﻿namespace minroute
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Points_CheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.Output_TextBox = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.WorkExcel_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ImportExcel_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExportExcel_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Add_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Edit_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Delete_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExportText_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Look2gis_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.About_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.Add_ContextMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Edit_ContextMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Delete_ContextMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Selection_ContextMenuStripItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Selection_CheckBox = new System.Windows.Forms.CheckBox();
            this.Address_CheckBox = new System.Windows.Forms.CheckBox();
            this.BuildRoute_Button = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Points_CheckedListBox
            // 
            this.Points_CheckedListBox.CheckOnClick = true;
            this.Points_CheckedListBox.ColumnWidth = 200;
            this.Points_CheckedListBox.FormattingEnabled = true;
            this.Points_CheckedListBox.Location = new System.Drawing.Point(20, 40);
            this.Points_CheckedListBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Points_CheckedListBox.MultiColumn = true;
            this.Points_CheckedListBox.Name = "Points_CheckedListBox";
            this.Points_CheckedListBox.Size = new System.Drawing.Size(605, 214);
            this.Points_CheckedListBox.TabIndex = 4;
            // 
            // Output_TextBox
            // 
            this.Output_TextBox.Location = new System.Drawing.Point(20, 306);
            this.Output_TextBox.Multiline = true;
            this.Output_TextBox.Name = "Output_TextBox";
            this.Output_TextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Output_TextBox.Size = new System.Drawing.Size(606, 300);
            this.Output_TextBox.TabIndex = 9;
            this.Output_TextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Output_TextBox_KeyPress);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Transparent;
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.WorkExcel_ToolStripMenuItem,
            this.Add_ToolStripMenuItem,
            this.Edit_ToolStripMenuItem,
            this.Delete_ToolStripMenuItem,
            this.ExportText_ToolStripMenuItem,
            this.Look2gis_ToolStripMenuItem,
            this.About_ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(650, 29);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // WorkExcel_ToolStripMenuItem
            // 
            this.WorkExcel_ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ImportExcel_ToolStripMenuItem,
            this.ExportExcel_ToolStripMenuItem});
            this.WorkExcel_ToolStripMenuItem.Name = "WorkExcel_ToolStripMenuItem";
            this.WorkExcel_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.WorkExcel_ToolStripMenuItem.Size = new System.Drawing.Size(121, 25);
            this.WorkExcel_ToolStripMenuItem.Text = "Работа с Excel";
            // 
            // ImportExcel_ToolStripMenuItem
            // 
            this.ImportExcel_ToolStripMenuItem.Name = "ImportExcel_ToolStripMenuItem";
            this.ImportExcel_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.ImportExcel_ToolStripMenuItem.Size = new System.Drawing.Size(237, 26);
            this.ImportExcel_ToolStripMenuItem.Text = "Импорт";
            this.ImportExcel_ToolStripMenuItem.Click += new System.EventHandler(this.ImportExcel_ToolStripMenuItem_Click);
            // 
            // ExportExcel_ToolStripMenuItem
            // 
            this.ExportExcel_ToolStripMenuItem.Enabled = false;
            this.ExportExcel_ToolStripMenuItem.Name = "ExportExcel_ToolStripMenuItem";
            this.ExportExcel_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.ExportExcel_ToolStripMenuItem.Size = new System.Drawing.Size(237, 26);
            this.ExportExcel_ToolStripMenuItem.Text = "Экспорт";
            this.ExportExcel_ToolStripMenuItem.Visible = false;
            this.ExportExcel_ToolStripMenuItem.Click += new System.EventHandler(this.ExportExcel_ToolStripMenuItem_Click);
            // 
            // Add_ToolStripMenuItem
            // 
            this.Add_ToolStripMenuItem.Name = "Add_ToolStripMenuItem";
            this.Add_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.Add_ToolStripMenuItem.Size = new System.Drawing.Size(61, 25);
            this.Add_ToolStripMenuItem.Text = "Плюс";
            this.Add_ToolStripMenuItem.Click += new System.EventHandler(this.Add_ToolStripMenuItem_Click);
            // 
            // Edit_ToolStripMenuItem
            // 
            this.Edit_ToolStripMenuItem.Name = "Edit_ToolStripMenuItem";
            this.Edit_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.Edit_ToolStripMenuItem.Size = new System.Drawing.Size(59, 25);
            this.Edit_ToolStripMenuItem.Text = "Перо";
            this.Edit_ToolStripMenuItem.Visible = false;
            this.Edit_ToolStripMenuItem.Click += new System.EventHandler(this.Edit_ToolStripMenuItem_Click);
            this.Edit_ToolStripMenuItem.VisibleChanged += new System.EventHandler(this.Edit_ToolStripMenuItem_VisibleChanged);
            // 
            // Delete_ToolStripMenuItem
            // 
            this.Delete_ToolStripMenuItem.Name = "Delete_ToolStripMenuItem";
            this.Delete_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.Delete_ToolStripMenuItem.Size = new System.Drawing.Size(59, 25);
            this.Delete_ToolStripMenuItem.Text = "Удал.";
            this.Delete_ToolStripMenuItem.Visible = false;
            this.Delete_ToolStripMenuItem.Click += new System.EventHandler(this.Delete_ToolStripMenuItem_Click);
            this.Delete_ToolStripMenuItem.VisibleChanged += new System.EventHandler(this.Delete_ToolStripMenuItem_VisibleChanged);
            // 
            // ExportText_ToolStripMenuItem
            // 
            this.ExportText_ToolStripMenuItem.Name = "ExportText_ToolStripMenuItem";
            this.ExportText_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.ExportText_ToolStripMenuItem.Size = new System.Drawing.Size(117, 25);
            this.ExportText_ToolStripMenuItem.Text = "Экспорт в .txt";
            this.ExportText_ToolStripMenuItem.Visible = false;
            this.ExportText_ToolStripMenuItem.Click += new System.EventHandler(this.ExportText_ToolStripMenuItem_Click);
            // 
            // Look2gis_ToolStripMenuItem
            // 
            this.Look2gis_ToolStripMenuItem.Name = "Look2gis_ToolStripMenuItem";
            this.Look2gis_ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.Look2gis_ToolStripMenuItem.Size = new System.Drawing.Size(136, 25);
            this.Look2gis_ToolStripMenuItem.Text = "Смотреть в 2gis";
            this.Look2gis_ToolStripMenuItem.Visible = false;
            this.Look2gis_ToolStripMenuItem.Click += new System.EventHandler(this.Look2gis_ToolStripMenuItem_Click);
            // 
            // About_ToolStripMenuItem
            // 
            this.About_ToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.About_ToolStripMenuItem.Name = "About_ToolStripMenuItem";
            this.About_ToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.About_ToolStripMenuItem.Size = new System.Drawing.Size(82, 25);
            this.About_ToolStripMenuItem.Text = "Справка";
            this.About_ToolStripMenuItem.Click += new System.EventHandler(this.About_ToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Add_ContextMenuStripItem,
            this.Edit_ContextMenuStripItem,
            this.Delete_ContextMenuStripItem,
            this.Selection_ContextMenuStripItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(155, 92);
            // 
            // Add_ContextMenuStripItem
            // 
            this.Add_ContextMenuStripItem.Name = "Add_ContextMenuStripItem";
            this.Add_ContextMenuStripItem.Size = new System.Drawing.Size(154, 22);
            this.Add_ContextMenuStripItem.Text = "Добавить";
            this.Add_ContextMenuStripItem.Click += new System.EventHandler(this.Add_ToolStripMenuItem_Click);
            // 
            // Edit_ContextMenuStripItem
            // 
            this.Edit_ContextMenuStripItem.Enabled = false;
            this.Edit_ContextMenuStripItem.Name = "Edit_ContextMenuStripItem";
            this.Edit_ContextMenuStripItem.Size = new System.Drawing.Size(154, 22);
            this.Edit_ContextMenuStripItem.Text = "Редактировать";
            this.Edit_ContextMenuStripItem.Click += new System.EventHandler(this.Edit_ToolStripMenuItem_Click);
            // 
            // Delete_ContextMenuStripItem
            // 
            this.Delete_ContextMenuStripItem.Enabled = false;
            this.Delete_ContextMenuStripItem.Name = "Delete_ContextMenuStripItem";
            this.Delete_ContextMenuStripItem.Size = new System.Drawing.Size(154, 22);
            this.Delete_ContextMenuStripItem.Text = "Удалить";
            this.Delete_ContextMenuStripItem.Click += new System.EventHandler(this.Delete_ToolStripMenuItem_Click);
            // 
            // Selection_ContextMenuStripItem
            // 
            this.Selection_ContextMenuStripItem.Name = "Selection_ContextMenuStripItem";
            this.Selection_ContextMenuStripItem.Size = new System.Drawing.Size(154, 22);
            this.Selection_ContextMenuStripItem.Text = "Выделить всё";
            this.Selection_ContextMenuStripItem.Click += new System.EventHandler(this.Selection_ContextMenuStripItem_Click);
            // 
            // Selection_CheckBox
            // 
            this.Selection_CheckBox.AutoSize = true;
            this.Selection_CheckBox.Checked = true;
            this.Selection_CheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Selection_CheckBox.Location = new System.Drawing.Point(20, 268);
            this.Selection_CheckBox.Name = "Selection_CheckBox";
            this.Selection_CheckBox.Size = new System.Drawing.Size(137, 24);
            this.Selection_CheckBox.TabIndex = 12;
            this.Selection_CheckBox.Text = "Выделить всё";
            this.Selection_CheckBox.UseVisualStyleBackColor = true;
            this.Selection_CheckBox.CheckedChanged += new System.EventHandler(this.Selection_CheckBox_CheckedChanged);
            // 
            // Address_CheckBox
            // 
            this.Address_CheckBox.AutoSize = true;
            this.Address_CheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Address_CheckBox.Location = new System.Drawing.Point(446, 268);
            this.Address_CheckBox.Name = "Address_CheckBox";
            this.Address_CheckBox.Size = new System.Drawing.Size(180, 24);
            this.Address_CheckBox.TabIndex = 13;
            this.Address_CheckBox.Text = "Показывать адреса";
            this.Address_CheckBox.UseVisualStyleBackColor = true;
            this.Address_CheckBox.CheckedChanged += new System.EventHandler(this.Address_CheckBox_CheckedChanged);
            // 
            // BuildRoute_Button
            // 
            this.BuildRoute_Button.Enabled = false;
            this.BuildRoute_Button.Location = new System.Drawing.Point(222, 264);
            this.BuildRoute_Button.Name = "BuildRoute_Button";
            this.BuildRoute_Button.Size = new System.Drawing.Size(189, 30);
            this.BuildRoute_Button.TabIndex = 15;
            this.BuildRoute_Button.Text = "Построить маршрут";
            this.BuildRoute_Button.UseVisualStyleBackColor = true;
            this.BuildRoute_Button.Click += new System.EventHandler(this.BuildRoute_Button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(650, 627);
            this.Controls.Add(this.BuildRoute_Button);
            this.Controls.Add(this.Address_CheckBox);
            this.Controls.Add(this.Selection_CheckBox);
            this.Controls.Add(this.Output_TextBox);
            this.Controls.Add(this.Points_CheckedListBox);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Как посетить всё?";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.CheckedListBox Points_CheckedListBox;
        public System.Windows.Forms.TextBox Output_TextBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem WorkExcel_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ImportExcel_ToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem ExportExcel_ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Add_ContextMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem Edit_ContextMenuStripItem;
        public System.Windows.Forms.ToolStripMenuItem Look2gis_ToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem ExportText_ToolStripMenuItem;
        public System.Windows.Forms.CheckBox Selection_CheckBox;
        public System.Windows.Forms.CheckBox Address_CheckBox;
        private System.Windows.Forms.ToolStripMenuItem Selection_ContextMenuStripItem;
        private System.Windows.Forms.ToolStripMenuItem Add_ToolStripMenuItem;
        public System.Windows.Forms.ToolStripMenuItem Edit_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem About_ToolStripMenuItem;
        public System.Windows.Forms.Button BuildRoute_Button;
        public System.Windows.Forms.ToolStripMenuItem Delete_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Delete_ContextMenuStripItem;
    }
}

