﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace minroute
{
    public partial class Form1 : Form
    {
        public List<List<double>> all_distances = new List<List<double>>();
        public List<string> all_points = new List<string>();
        public List<string[]> all_points_info = new List<string[]>();
        string url, big_route, lil_route;
        public Form1()
        {
            this.MaximizeBox = false;
            Program.form1 = this;
            InitializeComponent();
            Points_CheckedListBox.ContextMenuStrip = contextMenuStrip1;
            Add_ToolStripMenuItem.Text = "➕";
            Edit_ToolStripMenuItem.Text = "🖊";
            Delete_ToolStripMenuItem.Text = "🗑";
        }

        private void ImportExcel_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            delete_all();

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Файл Microsoft Office Excel (*.xls; *.xlsx)|*.xls;*.xlsx";
            ofd.Title = "Выберите файл для импорта данных";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                Excel.Application ex_application = new Excel.Application();
                Excel.Workbook ex_workbook = ex_application.Workbooks.Open(ofd.FileName);
                Excel.Worksheet ex_worksheet = ex_workbook.Sheets[1];

                try
                {
                    var last_cell = ex_worksheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell);
                    int last_column = (int)last_cell.Column;
                    int last_row = (int)last_cell.Row;
                    var data_array = (object[,])ex_worksheet.Range[ex_worksheet.Range["A1"], ex_worksheet.Cells[last_row, last_column]].Value;

                    if (last_row >= 2 && last_column >= 2)
                    {
                        for (int c = 2; c <= last_column; c++)
                        {
                            List<double> temp_list = new List<double>();
                            for (int r = 2; r <= last_row; r++)
                            {
                                string cell_text = data_array[c, r].ToString();
                                if (r == c)
                                    temp_list.Add(Double.PositiveInfinity);
                                else if (double.TryParse(cell_text, out double res))
                                    temp_list.Add(res);
                                else if (cell_text == "M" || cell_text == "М")
                                    temp_list.Add(Double.PositiveInfinity);
                                else
                                    throw new Exception("Ваш Excel-файл хранит недопустимые значения расстояния\nПровеьте формат записи данных");
                            }
                            all_distances.Add(temp_list);
                            if (data_array[1, c].ToString() == data_array[c, 1].ToString())
                                all_points.Add(data_array[1, c].ToString());
                            else
                                throw new Exception("Ваш Excel-файл хранит условные обозначения в недопустимом формате\nПровеьте формат записи данных");
                        }
                        if (ex_workbook.Sheets.Count >= 2)
                        {
                            Excel.Worksheet ex_info_sheet = (Excel.Worksheet)ex_workbook.Sheets[2];
                            var info_array = (object[,])ex_info_sheet.Range[ex_info_sheet.Range["A1"], ex_info_sheet.Cells[last_row, 3]].Value;
                            bool info_format_correct = true;
                            for (int r = 1; r < last_row; r++)
                            {
                                if (info_array[r, 1].ToString() == all_points[r - 1])
                                {
                                    string[] temp_array = new string[3];
                                    for (int c = 1; c < 4; c++)
                                        temp_array[c - 1] = info_array[r, c].ToString();
                                    all_points_info.Add(temp_array);
                                }
                                else
                                    info_format_correct = false;
                            }
                            if (!info_format_correct)
                            {
                                MessageBox.Show("Ваш Excel-файл хранит дополнительную информацию в недопустимом формате\nПрочие данные считаны, но впредь будьте аккуратнее", "Ай-ай");
                                all_points_info.Clear();
                            }
                        }
                        ExportExcel_ToolStripMenuItem.Visible = true;
                        ExportExcel_ToolStripMenuItem.Enabled = true;
                        Edit_ToolStripMenuItem.Visible = true;
                        Delete_ToolStripMenuItem.Visible = true;
                        BuildRoute_Button.Enabled = true; // !
                    }
                    else
                        throw new Exception("Ваш Excel-файл хранит недостаточно данных для для импорта");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ":с");
                }
                finally
                {
                    ex_workbook.Close(false, Type.Missing, Type.Missing);
                    ex_application.Quit();
                    ex_application = null;
                    print_all_points();
                }
            }
        }

        private void print_all_points()
        {
            Points_CheckedListBox.Items.Clear();
            if (!Address_CheckBox.Checked)
            {
                for (int i = 0; i < all_points.Count; i++)
                    Points_CheckedListBox.Items.Add(all_points[i], Selection_CheckBox.CheckState);
            }
            else
            {
                if (all_points_info.Count != 0)
                    for (int i = 0; i < all_points.Count; i++)
                        Points_CheckedListBox.Items.Add(all_points_info[i][1], Selection_CheckBox.CheckState);
                else
                    for (int i = 0; i < all_points.Count; i++)
                        Points_CheckedListBox.Items.Add("\"" + all_points[i] + "\"", Selection_CheckBox.CheckState);
            }
        }

        private void Add_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 add_form = new Form2(-1);
            add_form.Text = "Добавление";
            add_form.ShowDialog();
        }

        private void Edit_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Edit_ToolStripMenuItem.Visible)
            {
                if (Points_CheckedListBox.SelectedIndex != -1)
                {
                    Form2 edit_form = new Form2(Points_CheckedListBox.SelectedIndex);
                    edit_form.Text = "Редактирование " + Points_CheckedListBox.SelectedItem.ToString();
                    edit_form.ShowDialog();
                }
                else
                    MessageBox.Show("Нельзя редактировать невыделенный маршрут", "Обратите внимание");
            }
        }
        private void Edit_ToolStripMenuItem_VisibleChanged(object sender, EventArgs e)
        {
            Edit_ContextMenuStripItem.Enabled = Edit_ToolStripMenuItem.Visible;
        }

        private void Delete_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Delete_ToolStripMenuItem.Visible)
            {
                if (ModifierKeys != Keys.Shift)
                {
                    if (Points_CheckedListBox.SelectedIndex != -1)
                    {
                        List<List<double>> new_distances = new List<List<double>>();
                        List<string> new_points = new List<string>();
                        List<string[]> new_points_info = new List<string[]>();
                        for (int i = 0; i < all_distances.Count; i++)
                        {
                            if (i != Points_CheckedListBox.SelectedIndex)
                            {
                                new_points.Add(all_points[i]);
                                if (all_points_info.Count != 0)
                                    new_points_info.Add(all_points_info[i]);

                                List<double> temp_list = new List<double>();
                                for (int j = 0; j < all_distances.Count; j++)
                                {
                                    if (j != Points_CheckedListBox.SelectedIndex)
                                    {
                                        temp_list.Add(all_distances[i][j]);
                                    }
                                }
                                new_distances.Add(temp_list);
                            }
                        }
                        all_distances = new_distances;
                        all_points = new_points;
                        all_points_info = new_points_info;

                        Points_CheckedListBox.Items.Remove(Points_CheckedListBox.SelectedItem);
                        Output_TextBox.Text = "";
                        lil_route = "";
                        big_route = "";
                    }
                    else
                        MessageBox.Show("Нельзя удалить невыделенный пункт", "Обратите внимание");

                    if (Points_CheckedListBox.Items.Count == 0)
                        delete_all();
                }
                else
                    delete_all();
            }
        }
        public void delete_all()
        {
            all_distances.Clear();
            all_points.Clear();
            all_points_info.Clear();
            Points_CheckedListBox.Items.Clear();
            Output_TextBox.Text = "";
            lil_route = "";
            big_route = "";
            Edit_ToolStripMenuItem.Visible = false;
            Delete_ToolStripMenuItem.Visible = false;
            ExportExcel_ToolStripMenuItem.Visible = false;
            ExportExcel_ToolStripMenuItem.Enabled = false;
            BuildRoute_Button.Enabled = false; // !
            ExportText_ToolStripMenuItem.Visible = false;
            Look2gis_ToolStripMenuItem.Visible = false;
        }
        private void Delete_ToolStripMenuItem_VisibleChanged(object sender, EventArgs e)
        {
            Delete_ContextMenuStripItem.Enabled = Delete_ToolStripMenuItem.Visible;
        }

        private void BuildRoute_Button_Click(object sender, EventArgs e)
        {
            lil_route = "";
            big_route = "";
            if (Points_CheckedListBox.CheckedItems.Count != 0)
            {
                List<List<double>> selected_distances = new List<List<double>>();
                List<string> selected_points = new List<string>();
                List<string[]> selected_points_info = new List<string[]>();

                bool Ham_cycle = true;
                for (int i = 0; i < all_distances.Count; i++)
                {
                    if (Points_CheckedListBox.GetItemChecked(i))
                    {
                        selected_points.Add(all_points[i]);
                        if (all_points_info.Count != 0)
                            selected_points_info.Add(all_points_info[i]);

                        List<double> temp_list = new List<double>();
                        int num_non_inf = 0;
                        for (int j = 0; j < all_distances.Count; j++)
                        {
                            if (Points_CheckedListBox.GetItemChecked(j))
                            {
                                temp_list.Add(all_distances[i][j]);
                                if (!double.IsInfinity(all_distances[i][j]))
                                    num_non_inf++;
                            }
                        }
                        if (num_non_inf < 2)
                            Ham_cycle = false;
                        selected_distances.Add(temp_list);
                    }
                }

                if (Ham_cycle)
                {
                    Graph graph = new Graph(selected_distances);
                    System.Diagnostics.Stopwatch clock = new System.Diagnostics.Stopwatch();

                    clock.Start();
                    List<Graph.Index> way = graph.Solve();
                    clock.Stop();

                    double whole_way = 0;
                    Console.WriteLine(selected_points.Count);
                    lil_route = selected_points[way[0].from].ToString();
                    if (all_points_info.Count == all_points.Count) // Есть адреса
                    {
                        big_route = selected_points[way[0].from].ToString() + " (" + selected_points_info[way[0].from][1] + ")";
                        for (int i = 0; i < way.Count; i++)
                        {
                            double temp_dist = selected_distances[way[i].from][way[i].to];
                            lil_route += " > [" + temp_dist + "] > " + selected_points[way[i].to].ToString();
                            big_route += " > [" + temp_dist + "] > " + selected_points[way[i].to].ToString();
                            string address = selected_points_info[way[i].to][1];
                            if (address.Length > 0)
                                big_route += " (" + address + ")";
                            whole_way += temp_dist;
                        }
                    }
                    else // Нет адресов
                    {
                        for (int i = 0; i < way.Count; i++)
                        {
                            double temp_dist = selected_distances[way[i].from][way[i].to];
                            lil_route += " > [" + temp_dist + "] > " + selected_points[way[i].to].ToString();
                            whole_way += temp_dist;
                        }
                        big_route = lil_route;
                    }
                    lil_route += "\r\n\r\nРасстояние: " + whole_way + "\r\nВремя выполнения: " + clock.ElapsedMilliseconds + " мс.";
                    big_route += "\r\n\r\nРасстояние: " + whole_way + "\r\nВремя выполнения: " + clock.ElapsedMilliseconds + " мс.";

                    bool info_status = false;
                    if (all_points_info.Count != 0)
                    {
                        info_status = false;

                        if (Points_CheckedListBox.CheckedItems.Count <= 10)
                            url = "https://2gis.ru/tyumen/directions/tab/pedestrian/points/";
                        else
                            url = "https://2gis.ru/?m=";

                        string[] first_point_coords = selected_points_info[way[0].from][2].Split(',');
                        if (first_point_coords.Length == 2)
                            if (double.TryParse(first_point_coords[0].Replace(".", ","), out double resx) && double.TryParse(first_point_coords[1].Replace(".", ","), out double resy))
                            {
                                if (Points_CheckedListBox.CheckedItems.Count <= 10) 
                                    url += selected_points_info[way[0].from][2]; // Работает не идеально. А жаль
                                else
                                    url += selected_points_info[way[0].from][2] + "%2F16&ruler=" + selected_points_info[way[0].from][2];
                                info_status = true;
                            }

                        if (info_status)
                            for (int i = 0; i < way.Count; i++)
                            {
                                string[] other_point_coords = selected_points_info[way[i].to][2].Split(',');
                                if (other_point_coords.Length == 2)
                                    if (double.TryParse(other_point_coords[0].Replace(".", ","), out double resx) && double.TryParse(other_point_coords[1].Replace(".", ","), out double resy))
                                        if (Points_CheckedListBox.CheckedItems.Count <= 10)
                                        {
                                            if (i != way.Count - 1)
                                                url += "|" + selected_points_info[way[i].to][2];
                                            else
                                                url += "|" + (resx - (double)1 / 10000).ToString().Replace(",", ".") + "," + (resy - (double)1/10000).ToString().Replace(",", ".");
                                               
                                        }
                                        else
                                            url += "|" + selected_points_info[way[i].to][2];
                                    else
                                        info_status = false;
                                else
                                    info_status = false;
                            }
                    }
                    Look2gis_ToolStripMenuItem.Visible = info_status;
                }
                else
                    MessageBox.Show("Невозможно построить маршрут на графе без Гамильтова цикла", "Внимание!");
            }
            else
                MessageBox.Show("Нельзя построить маршрут по невыделенным точкам", "Обратите внимание");

            if (Address_CheckBox.Checked)
                Output_TextBox.Text = big_route;
            else
                Output_TextBox.Text = lil_route;

            ExportText_ToolStripMenuItem.Visible = (Output_TextBox.Text != "");
        }

        private void Look2gis_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (Look2gis_ToolStripMenuItem.Visible)
                System.Diagnostics.Process.Start(url);
        }

        private void ExportExcel_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ExportExcel_ToolStripMenuItem.Enabled) // проблема с Visible
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "Файл Microsoft Office Excel (*.xlsx)|*.xlsx";
                sfd.Title = "Выберите файл для экспорта данных";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    Excel.Application ex_application = new Excel.Application();
                    ex_application.Visible = false;
                    Excel.Workbook ex_workbook = ex_application.Workbooks.Add(Type.Missing);
                    ex_application.DisplayAlerts = false;

                    try
                    {
                        if (all_points_info.Count == all_points.Count)
                        {
                            Excel.Worksheet ex_infosheet = (Excel.Worksheet)ex_workbook.Worksheets[1];
                            ex_infosheet.Name = "Informations";
                            for (int c = 1; c <= all_points_info.Count; c++)
                                for (int r = 1; r <= all_points_info[c - 1].Length; r++)
                                    if (r == 2)
                                    {
                                        if (all_points_info[c - 1][r - 1] != $"\"{all_points[c - 1]}\"")
                                            ex_infosheet.Cells[c, r] = all_points_info[c - 1][r - 1];
                                    }
                                    else
                                        ex_infosheet.Cells[c, r] = all_points_info[c - 1][r - 1];
                            ex_workbook.Sheets.Add();
                        }

                        Excel.Worksheet ex_pointssheet = (Excel.Worksheet)ex_workbook.Worksheets[1];
                        ex_pointssheet.Name = "Routes";
                        int i;
                        for (i = 0; i < Points_CheckedListBox.Items.Count; i++)
                        {
                            ex_pointssheet.Cells[1, i + 2] = ex_pointssheet.Cells[i + 2, 1] = all_points[i];
                        }
                        i = 2;
                        foreach (var lst in all_distances)
                        {
                            int j = 2;
                            foreach (var route in lst)
                            {
                                if (route != Double.PositiveInfinity)
                                    ex_pointssheet.Cells[i, j] = route;
                                else
                                    ex_pointssheet.Cells[i, j] = "M";
                                j++;
                            }
                            i++;
                        }
                        ex_application.Application.ActiveWorkbook.SaveAs(sfd.FileName);
                        ex_application.Quit();
                        ex_application = null;
                        MessageBox.Show("Пути успешно экспортированы в Excel", "Экспорт");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, ":с");
                        ex_application.Quit();
                        ex_application = null;
                    }
                }
            }
        }

        private void ExportText_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ExportText_ToolStripMenuItem.Visible)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.DefaultExt = "*.txt";
                sfd.Filter = "Текстовый файл (*.txt)|*.txt";
                sfd.Title = "Выберите файл для экспорта полученного маршрута";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    System.IO.File.WriteAllText(sfd.FileName, Output_TextBox.Text);
                    MessageBox.Show("Маршрут успешно экспортирован в текстовый файл", "Экспорт");
                }
            }
        }

        private void Selection_ContextMenuStripItem_Click(object sender, EventArgs e)
        {
            Selection_CheckBox.Checked = !Selection_CheckBox.Checked;
        }
        private void Selection_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < Points_CheckedListBox.Items.Count; i++)
                Points_CheckedListBox.SetItemCheckState(i, Selection_CheckBox.CheckState);
        }

        private void Address_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (BuildRoute_Button.Enabled)
            {
                print_all_points();
                if (Output_TextBox.Text != "")
                    if (Address_CheckBox.Checked)
                        Output_TextBox.Text = big_route;
                    else
                        Output_TextBox.Text = lil_route;
            }
        }

        private void About_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.ShowDialog();
        }

        private void Output_TextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
    }
}