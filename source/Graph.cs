﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace minroute
{
    class Graph
    {
        /*
         * В конструктор передается весовая матрица типа List<List<double>>. 
         * Метод Solve(), построив дерево решений по Методу ветвей и границ, составляет список выгодных путей типа Graph.Index.
         * Graph.Index имеет свойства: from (откуда), to (куда).
         */

        Graph() { }

        public Graph(List<List<double>> graph)
        {
            route = new List<Index>();
            for (int i = 0; i < graph.Count; i++)
            {
                List<Edge> vector = new List<Edge>();
                for (int j = 0; j < graph[i].Count; j++)
                {
                    vector.Add(new Edge() { value = graph[i][j], index = new Index(i, j) });
                }
                this.graph.Add(vector);
            }
            Reduction();
            DefineIndexOfZeroWithMaxMark();
        }

        public List<Index> Solve()
        {
            Graph active = this;
            while (active.Count > 0)
            {
                try
                {
                    active.createLeft(active.indexOfZeroWithMaxMark);
                    active.createRight(active.indexOfZeroWithMaxMark);
                }
                catch
                {
                    active.left = new Graph() { Mark = Double.PositiveInfinity };
                    active.right = new Graph() { Mark = Double.PositiveInfinity };
                }
                active = minMarkGraph();
            }
            return Sorting(active.route);
        }

        List<Index> Sorting(List<Index> unsortedRoute)
        {
            List<Index> sortedRoute = new List<Index>();
            int from = 0;
            for (int i = 0; i < unsortedRoute.Count; i++)
            {
                if (unsortedRoute[i].from == 0)
                {
                    from = unsortedRoute[i].to;
                    sortedRoute.Add(unsortedRoute[i]);
                    break;
                }
            }
            while (unsortedRoute.Count != sortedRoute.Count)
            {
                for (int i = 0; i < unsortedRoute.Count; i++)
                {
                    if (unsortedRoute[i].from == from)
                    {
                        from = unsortedRoute[i].to;
                        sortedRoute.Add(unsortedRoute[i]);
                        break;
                    }
                }
            }
            return sortedRoute;
        }

        public class Index
        {
            public Index(int i, int j)
            {
                from = i; to = j;
            }
            public int from { get; private set; }
            public int to { get; private set; }
            public Index Inverse
            {
                get
                {
                    return new Index(to, from);
                }
            }
            public static bool operator ==(Index a, Index b)
            {
                return a.from == b.from && a.to == b.to;
            }
            public static bool operator !=(Index a, Index b)
            {
                return a.from != b.from || a.to != b.to;
            }
        }

        class Edge
        {
            public double value;
            public Index index;
        }

        List<List<Edge>> graph = new List<List<Edge>>();

        int Count { get { return graph.Count; } }

        Graph left, right;
        bool isLeft;
        Index indexOfZeroWithMaxMark;
        double Mark;

        List<Index> route;

        Graph minMarkGraph()
        {
            Graph g = this;
            Graph min = new Graph() { Mark = Double.PositiveInfinity };
            Queue<Graph> queue = new Queue<Graph>();
            do
            {
                if (g.left == null && min.Mark > g.Mark)
                {
                    min = g;
                }
                if (g.left != null)
                {
                    queue.Enqueue(g.left);
                }
                if (g.right != null)
                {
                    queue.Enqueue(g.right);
                }
                if (queue.Count > 0)
                {
                    g = queue.Dequeue();
                }
            } while (queue.Count > 0);
            return min;
        }

        void createLeft(Index maxMark)
        {
            left = new Graph() { isLeft = true, Mark = this.Mark, route = new List<Index>() };
            left.route.AddRange(this.route);
            left.route.Add(maxMark);
            for (int i = 0; i < Count; i++)
            {
                if (graph[i][0].index.from == maxMark.from)
                {
                    continue;
                }
                List<Edge> vector = new List<Edge>();
                for (int j = 0; j < Count; j++)
                {
                    if (graph[0][j].index.to == maxMark.to)
                    {
                        continue;
                    }
                    if (graph[i][j].index == maxMark.Inverse)
                    {
                        vector.Add(new Edge() { value = Double.PositiveInfinity, index = graph[i][j].index });
                    }
                    else
                    {
                        vector.Add(new Edge() { value = graph[i][j].value, index = graph[i][j].index });
                    }
                }
                left.graph.Add(vector);
            }
            left.Reduction();
            left.DefineIndexOfZeroWithMaxMark();
        }

        void createRight(Index maxMark)
        {
            right = new Graph() { Mark = this.Mark, route = new List<Index>() };
            right.route.AddRange(this.route);
            for (int i = 0; i < Count; i++)
            {
                List<Edge> vector = new List<Edge>();
                for (int j = 0; j < Count; j++)
                {
                    if (graph[i][j].index == maxMark)
                    {
                        vector.Add(new Edge() { value = Double.PositiveInfinity, index = graph[i][j].index });
                    }
                    else
                    {
                        vector.Add(new Edge() { value = graph[i][j].value, index = graph[i][j].index });
                    }
                }
                right.graph.Add(vector);
            }
            right.Reduction();
            right.DefineIndexOfZeroWithMaxMark();
        }

        void Reduction()
        {
            double[] constsInRows = new double[Count];
            double[] constsInColumns = new double[Count];
            for (int i = 0; i < Count; i++)
            {
                constsInRows[i] = graph[i][0].value;
                for (int j = 1; j < Count; j++)
                {
                    if (constsInRows[i] > graph[i][j].value)
                    {
                        constsInRows[i] = graph[i][j].value;
                    }
                }
                for (int j = 0; j < Count; j++)
                {
                    graph[i][j].value -= constsInRows[i];
                }
            }
            for (int i = 0; i < Count; i++)
            {
                constsInColumns[i] = graph[0][i].value;
                for (int j = 1; j < Count; j++)
                {
                    if (constsInColumns[i] > graph[j][i].value)
                    {
                        constsInColumns[i] = graph[j][i].value;
                    }
                }
                for (int j = 0; j < Count; j++)
                {
                    graph[j][i].value -= constsInColumns[i];
                }
            }
            double sum = 0;
            for (int i = 0; i < Count; i++)
            {
                sum += constsInRows[i] + constsInColumns[i];
            }
            Mark += sum;
        }

        void DefineIndexOfZeroWithMaxMark()
        {
            double mark = -1;
            for (int i = 0; i < Count; i++)
            {
                for (int j = 0; j < Count; j++)
                {
                    if (graph[i][j].value == 0 && littleCycle(graph[i][j].index))
                    {
                        graph[i][j].value = Double.PositiveInfinity;
                    }
                    else if (graph[i][j].value == 0)
                    {
                        double minInRow = Double.PositiveInfinity;
                        double minInColumn = minInRow;
                        for (int k = 0; k < Count; k++)
                        {
                            if (k != j && minInRow > graph[i][k].value)
                            {
                                minInRow = graph[i][k].value;
                            }
                        }
                        for (int k = 0; k < Count; k++)
                        {
                            if (k != i && minInColumn > graph[k][j].value)
                            {
                                minInColumn = graph[k][j].value;
                            }
                        }
                        if (mark < minInRow + minInColumn)
                        {
                            mark = minInRow + minInColumn;
                            indexOfZeroWithMaxMark = graph[i][j].index;
                        }
                    }
                }
            }
        }

        bool littleCycle(Index index)
        {
            if (!isLeft)
            {
                return false;
            }
            if (Count == 1)
            {
                return false;
            }
            int current = index.to;
            int danger = index.from;
            for (int i = 0; i < route.Count; i++)
            {
                foreach (Index a in route)
                {
                    if (current == a.from)
                    {
                        current = a.to;
                        break;
                    }
                }
                if (current == danger)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
